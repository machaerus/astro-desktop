from matplotlib import pyplot as plt
from matplotlib.collections import LineCollection
import matplotlib.font_manager as mfm
import numpy as np
import pandas as pd
from skyfield.api import Star, load, wgs84
from skyfield.magnitudelib import planetary_magnitude
from skyfield.data import hipparcos, stellarium
from skyfield.projections import build_stereographic_projection
from thelemic_date import ThelemicDate

# Use the test_fonts.py script to find which fonts support the symbols
# we need. You should be able to use DejaVu Sans font which comes preinstalled
# with matplotlib, though it is not the prettiest.
font_paths = [
	"/usr/share/fonts/noto/NotoSansSymbols-Regular.ttf",
	"/usr/share/fonts/noto/NotoSansSymbols2-Regular.ttf",
	"/home/machaerus/.fonts/Pragmata Pro/Pragmata Pro Regular.otf",
	"/home/machaerus/.fonts/evermono-7.0.0/Everson Mono Bold.ttf"
]


def plot_date(ax, location_name):
	td = ThelemicDate()
	current_date = td.now(location_name)
	prop = mfm.FontProperties(fname=font_paths[2])
	ax.text(
		-0.95, 0.8,
		s=current_date,
		fontproperties=prop,
		fontsize=20,
		color="white"
	)

def get_projection(earth, geo_coords, t):
	observer = wgs84.latlon(*geo_coords).at(t)
	ra, dec, _ = observer.radec()
	center_object = Star(ra=ra, dec=dec)
	center = earth.at(t).observe(center_object)
	projection = build_stereographic_projection(center)
	return projection

def get_constellation_lines(stars: pd.DataFrame):
	"""
	"""
	url = ('https://raw.githubusercontent.com/Stellarium/stellarium/master'
		'/skycultures/modern_st/constellationship.fab')

	with load.open(url) as f:
		constellations = stellarium.parse_constellations(f)

	edges = [edge for name, edges in constellations for edge in edges]
	edges_star1 = [star1 for star1, star2 in edges]
	edges_star2 = [star2 for star1, star2 in edges]

	# The constellation lines will each begin at the x,y of one star and end
	# at the x,y of another.  We have to "rollaxis" the resulting coordinate
	# array into the shape that matplotlib expects.
	xy1 = stars[['x', 'y']].loc[edges_star1].values
	xy2 = stars[['x', 'y']].loc[edges_star2].values
	lines_xy = np.rollaxis(np.array([xy1, xy2]), 1)
	return lines_xy


class Planet:
	def __init__(self, eph, name: str, symbol: str, color: str, font_file: int):
		self.name = name
		self.symbol = symbol
		self.color = color
		self.planet = eph[self.name]
		# Font to use
		self.prop = mfm.FontProperties(fname=font_paths[font_file])

	def coords(self, proj, point):
		return proj(point.observe(self.planet))

	def plot(self, ax, proj, point):
		x, y = self.coords(proj, point)
		if -1 < x < 1 and -1 < y < 1:
			ax.text(
				x, y,
				s=self.symbol,
				fontproperties=self.prop,
				fontsize=50,
				color=self.color
			)


def main():
	# Stars
	with load.open(hipparcos.URL) as f:
		stars = hipparcos.load_dataframe(f)
	limiting_magnitude = 8.0
	bright_stars = stars[stars.magnitude <= limiting_magnitude]

	# Planets
	eph = load('de421.bsp')
	planets = [
		Planet(eph, "sun", "☉", "gold", 3),
		Planet(eph, "moon", "☾", "white", 3),
		Planet(eph, "mars", "♂", "crimson", 3),
		Planet(eph, "venus", "♀", "crimson", 3),
		Planet(eph, "mercury", "☿", "crimson", 3),
		Planet(eph, "jupiter barycenter", "♃", "crimson", 3),
		Planet(eph, "saturn barycenter", "♄", "crimson", 3),
		Planet(eph, "neptune barycenter", "♆", "crimson", 3),
		Planet(eph, "uranus barycenter", "⛢", "crimson", 3),
		Planet(eph, "pluto barycenter", "⯓", "crimson", 3),
	]

	# Setting
	earth = eph['earth']
	ts = load.timescale()
	t = ts.now()
	# For Copenhagen
	geo_coords = (+55.6868, +12.5739)
	location_name = "Copenhagen, Denmark"
	location = (earth + wgs84.latlon(*geo_coords)).at(t)

	projection = get_projection(earth, geo_coords, t)
	star_positions = earth.at(t).observe(Star.from_dataframe(bright_stars))
	bright_stars['x'], bright_stars['y'] = projection(star_positions)
	marker_size = (0.2 + limiting_magnitude - bright_stars['magnitude']) ** 2.0

	constellation_lines = get_constellation_lines(bright_stars)

	# 4K = 3840×2160 px
	px = 1 / plt.rcParams['figure.dpi']  # pixel in inches
	fig, ax = plt.subplots(figsize=(3840*px, 2160*px))
	ax.add_collection(
		LineCollection(
			constellation_lines,
			colors='#5b7187',
			alpha=0.8
		)
	)
	ax.scatter(
		bright_stars['x'],
		bright_stars['y'],
		s=marker_size,
		c="#e0ca4e",
		alpha=0.8,
		zorder=1000
	)
	for planet in planets:
		planet.plot(ax, projection, location)

	plot_date(ax, location_name)

	# Limit the field of view
	# field_of_view_degrees = 45.0
	# angle = np.pi - field_of_view_degrees / 360.0 * np.pi
	# limit = np.sin(angle) / (1.0 - np.cos(angle))
	# ax.set_xlim(-limit, limit)
	# ax.set_ylim(-limit, limit)

	ax.set_xlim(-1, 1)
	ax.set_ylim(-1, 1)
	ax.xaxis.set_visible(False)
	ax.yaxis.set_visible(False)

	# To make it round:
	#
	# horizon = plt.Circle((0, 0), radius=1, transform=ax.transData)
	# for col in ax.collections:
	# 	col.set_clip_path(horizon)
	# ax.set_aspect(1.0)

	plt.axis("off")
	plt.savefig(
		"/home/machaerus/stars.png",
		bbox_inches="tight",
		pad_inches=0,
		transparent=True,
		dpi=150
	)


if __name__ == "__main__":
	main()
