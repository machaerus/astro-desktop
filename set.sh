#!/bin/bash

OLDPID=$(cat pid)
# swaybg -i ~/stars.png -m fit &
# PID=$!

# Create a new version
poetry run python plot_stars.py
swaybg -i ~/stars.png -m fit &
NEWPID=$!
echo $NEWPID > pid
kill $OLDPID
